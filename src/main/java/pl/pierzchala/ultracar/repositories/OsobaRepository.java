package pl.pierzchala.ultracar.repositories;


import pl.pierzchala.ultracar.models.Osoba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OsobaRepository extends JpaRepository<Osoba, Long> {

    @Query("select o from Osoba o where o.numerDowoduOsobistego=?1")
    Osoba findByNumerDowodu(String numerDowodu);
}
