package pl.pierzchala.ultracar.repositories;


import pl.pierzchala.ultracar.models.Konto;
import pl.pierzchala.ultracar.models.Osoba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface KontoRepository extends JpaRepository<Konto, Long> {

    @Query("select k from Konto k where ?1=k.daneLogowania")
    Konto findByCredentials(String daneLogowania);

    @Query("select k from Konto k where ?1=k.osoba")
    Konto findByUser(Osoba osoby);

    @Query("select k.osoba from Konto k where ?1=k.daneLogowania")
    Osoba findUserByCredentials(String poswiadczeniaUzytkownika);
}
