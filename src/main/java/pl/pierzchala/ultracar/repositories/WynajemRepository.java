package pl.pierzchala.ultracar.repositories;


import pl.pierzchala.ultracar.models.Osoba;
import pl.pierzchala.ultracar.models.Pojazd;
import pl.pierzchala.ultracar.models.Wynajem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface WynajemRepository extends JpaRepository<Wynajem, Long> {

    @Query("select r from Wynajem r where r.klient=?1")
    List<Wynajem> findByClient(Osoba osoby);

    @Transactional
    @Modifying
    @Query("delete from Wynajem r where r.pojazd=?1")
    void deleteRentBasedOnCar(Pojazd pojazd);
}
