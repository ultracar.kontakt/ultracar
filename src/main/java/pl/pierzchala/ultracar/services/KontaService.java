package pl.pierzchala.ultracar.services;

import lombok.RequiredArgsConstructor;
import pl.pierzchala.ultracar.models.Konto;
import pl.pierzchala.ultracar.repositories.KontoRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class KontaService {

    private final KontoRepository kontaRepository;

    public void saveAccountToDB(Konto konto){
        if (kontaRepository.findAll().size()==0){
            konto.setRola("BOSS");
        }
        kontaRepository.save(konto);
    }
}
