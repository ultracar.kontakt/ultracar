package pl.pierzchala.ultracar.models;

import javax.persistence.*;


@Entity
public class Konto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_osoby", referencedColumnName = "id")
    private Osoba osoba;

    @Column(name = "dane_logowania")
    private String daneLogowania;

    @Column(name = "rola")
    private String rola;

    public Konto() {
    }

    public Konto(Osoba osoba, String daneLogowania, String rola) {
        this.osoba = osoba;
        this.daneLogowania = daneLogowania;
        this.rola = rola;
    }

    public Osoba getOsoba() {
        return osoba;
    }

    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
    }

    public String getDaneLogowania() {
        return daneLogowania;
    }

    public void setRola(String rola) {
        this.rola = rola;
    }

    public String getRola() {
        return rola;
    }
}
