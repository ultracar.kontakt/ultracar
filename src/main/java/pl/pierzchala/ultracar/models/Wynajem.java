package pl.pierzchala.ultracar.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "wynajmy")
//@RequiredArgsConstructor()
public class Wynajem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "id_pojazdu", referencedColumnName = "id")
    private Pojazd pojazd;

    @OneToOne
    @JoinColumn(name = "id_klienta", referencedColumnName = "id")
    private Osoba klient;

    @Column(name = "ilosc_dni_wynajmu")
    private LocalDate iloscDniWynajmu;

    public Wynajem(Pojazd pojazd, Osoba klient, LocalDate iloscDniWynajmu) {
        this.pojazd = pojazd;
        this.klient = klient;
        this.iloscDniWynajmu = iloscDniWynajmu;
    }

    public Wynajem() {
    }

    public long getId() {
        return id;
    }

    public Pojazd getPojazd() {
        return pojazd;
    }

    public Osoba getKlient() {
        return klient;
    }

    public LocalDate getIloscDniWynajmu() {
        return iloscDniWynajmu;
    }
}
