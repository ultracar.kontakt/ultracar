package pl.pierzchala.ultracar;



import pl.pierzchala.ultracar.Dto.UserData;

import java.util.Base64;

public class Base64Utils {
    public static String encodeToBase64(UserData userData){
        String poswiadczenia = String.format("%s:%s", userData.getLogin(), userData.getHaslo());
        return Base64.getEncoder().encodeToString(poswiadczenia.getBytes());
    }
}
