package pl.pierzchala.ultracar;

import pl.pierzchala.ultracar.Dto.UserData;
import pl.pierzchala.ultracar.models.Konto;
import pl.pierzchala.ultracar.models.Osoba;
import pl.pierzchala.ultracar.models.Pojazd;
import pl.pierzchala.ultracar.repositories.PojazdRepository;
import pl.pierzchala.ultracar.repositories.OsobaRepository;
import pl.pierzchala.ultracar.services.KontaService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
// spring podczas uiruchamiania aplikacji skanuje wszystkie pakunki projektu w poszukiwaniu klas oznaczonych @component/service/repository/controller
public class DbInitializer implements CommandLineRunner { // commandlinerunner - sluzy do uruchamiania @componentow razem ze startem aplikacji

    private final OsobaRepository userRepository;
    private final PojazdRepository pojazdRepository;
    private final KontaService kontaService;

    public DbInitializer(OsobaRepository userRepository, PojazdRepository pojazdRepository, KontaService kontaService) {// konstruktor wywolany przez kontener springa
        this.userRepository = userRepository;
        this.pojazdRepository = pojazdRepository;
        this.kontaService = kontaService;
    }

    @Override
    public void run(String... args) throws Exception { //metoda interfejsu commandLineRunner uruchamiająca się razem ze stratem aplikacji
        List<Osoba> osoby = loadPersonToDb();
        loadCarsToDb();
        loadAccountsToDb(osoby);
    }

    private List<Osoba> loadPersonToDb() {

        Osoba osoba1 = new Osoba();
        osoba1.setImie("Robert");
        osoba1.setNazwisko("Wiatrak");
        osoba1.setNumerDowoduOsobistego("AWK12345");
        osoba1.setNumerTelefonu("876653465");

        Osoba osoba2 = new Osoba();
        osoba2.setImie("Marlena");
        osoba2.setNazwisko("Michalak");
        osoba2.setNumerDowoduOsobistego("AWK743246");
        osoba2.setNumerTelefonu("812324543");

        Osoba osoba3 = new Osoba();
        osoba3.setImie("Karol");
        osoba3.setNazwisko("Dyć");
        osoba3.setNumerDowoduOsobistego("AWK67867");
        osoba3.setNumerTelefonu("509887765");
        List<Osoba> osoby = Arrays.asList(osoba1, osoba2, osoba3);
        userRepository.saveAll(osoby);

        return osoby;
    }

    private void loadCarsToDb() {
        Pojazd pojazd1 = new Pojazd();
        pojazd1.setMarka("Volkswagen");
        pojazd1.setModel("GOLF 4 R32");
        pojazd1.setMoc("241");
        pojazd1.setSrednieSpalanie("12");
        pojazd1.setCenaZaDobe("150");
        pojazd1.setMiniaturka("https://pictures.trader.pl/eurotax/pictures/8/4f/1396d860f80442f4dd43fc89c8fa2a26.jpg");
        pojazdRepository.save(pojazd1);
    }


    private void loadAccountsToDb(List<Osoba> osoby) {
        osoby.forEach(osoba -> {
            Konto konta = new Konto(osoba, Base64Utils.encodeToBase64(new UserData(osoba.getImie(), osoba.getNazwisko(), osoba.getImie().toLowerCase(), osoba.getNazwisko().toLowerCase(), osoba.getNumerDowoduOsobistego(), osoba.getNumerTelefonu())), "CLIENT");
            kontaService.saveAccountToDB(konta);
        });
    }

}
